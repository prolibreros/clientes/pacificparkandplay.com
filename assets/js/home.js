/*
 * Fixes home nav links, so instead of pointing to other pages, they point to
 * sections in the same document
 */
document.querySelectorAll("nav a.smooth-scroll").forEach((link) => {
  let id = link.pathname.split("/").at(-1).split(".")[0];
  link.href = "home.html#" + id;
});


