// Changes scroll behavior
function init_scroll () {
  let btns = document.querySelectorAll('.smooth-scroll');
  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener('click', function (e) {
      let a = e.target.nodeName == 'A' ? e.target : e.target.parentNode,
          id = a.href.split('#')[a.href.split('#').length - 1],
          sec = document.getElementById(id),
          offset = 49, // nav-height - 1 (section border)
          position = sec.getBoundingClientRect().top + window.pageYOffset - offset,
          menu_btn = document.getElementById('menu-btn');
      e.preventDefault();
      window.scrollTo({ top: position, behavior: "smooth" });
      if (menu_btn.classList.contains('menu-active')) {
        toggle_menu(menu_btn);
      }
    });
  }
}

// Adds function to menu button
function init_menu () {
  ["menu-btn", "menu-background"].forEach((id) => {
    let el = document.getElementById(id),
      // Cfr. https://stackoverflow.com/a/52855084
      isTouch = window.matchMedia("(pointer: coarse)").matches,
      listener = isTouch ? "touchstart" : "click";
    el.addEventListener(listener, toggle_menu);
  });
}

// Shows or hides menu bar
function toggle_menu () {
  let btn = document.getElementById('menu-btn'),
      menu = document.getElementById('menu'),
      background = document.getElementById('menu-background');
  btn.classList.toggle('menu-active');
  menu.classList.toggle('menu-active');
  background.classList.toggle('menu-active');
}

init_scroll();
init_menu();
