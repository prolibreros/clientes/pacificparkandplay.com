// Adds gallery
function init_gallery () {
  var gallery = document.getElementById("gallery-container"),
    headers = JSON.parse(gallery.dataset.headers);
  if (document.body.id == "home") {
    headers = Object.fromEntries(Object.entries(headers).slice(0, 4))
  }
  for (const [key, title] of Object.entries(headers)) {
    let images = pick_images(key, gallery.dataset.images.split(", "));
    if (images.length > 0) {
      add_gallery(gallery, images, key, title);
    }
  }
}

function pick_images(root, images) {
  var selected = [];
  images.forEach(img => {
    if (img.split("/")[3] == root) {
      selected.push({"full": `${img}full.jpg`, "thumb": `${img}thumb.jpg`})
    }
  });
  return selected;
}

function add_gallery(gallery, images, id, title) {
  let container = document.createElement("div"),
    header = document.createElement("h2");
  gallery.appendChild(header);
  gallery.appendChild(container);
  container.id = `gallery-${id}`;
  container.classList.add("gallery");
  header.innerText = title;
  add_images(container, images);
  lightGallery(container, {
    plugins: [lgZoom, lgThumbnail],
    thumbnail: true,
  });
}

function add_images(container, images) {
  images.forEach(img => {
    let html = `
    <a href="${img.full}">
        <img src="${img.thumb}" />
    </a>
    `
    container.insertAdjacentHTML('beforeend', html);
  });
}

init_gallery();
