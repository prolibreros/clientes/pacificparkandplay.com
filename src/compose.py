import re
import yaml
import json
import shutil
import pandas as pd
from pathlib import Path
from bs4 import BeautifulSoup, Comment

ROOT = Path(__file__).parent.parent
META = ROOT / "metadata.yaml"
YAML = yaml.safe_load(META.read_text())
PAGES = {p.name: p.read_text() for p in (ROOT / "templates").glob("**/page.*")}
ASSETS = ROOT / "assets"
PUBLIC = ROOT / "public"


def get_meta(keys):
    flat = pd.json_normalize(YAML, sep=":").to_dict(orient="records")[0]
    for key, val in flat.items():
        if isinstance(val, list):
            flat[key] = ", ".join(map(lambda v: str(v), val))
        else:
            flat[key] = str(val)
    for key, val in flat.items():
        flat[key] = add_meta(val, flat, META.name, keys)
    return flat


def add_include(content, meta, origin, keys):
    rgx = r"\$i:(?P<key>[\w\/\.]+)"
    content = add_meta(content, meta, origin, keys)
    for match in re.finditer(rgx, content):
        path = Path(ROOT / match["key"])
        if path.exists():
            include = add_meta(path.read_text(), meta, path.name, keys)
            keys["i"].setdefault(path, include)
            content = content.replace(match[0], keys["i"][path])
        else:
            print("[%s]: Key '%s' not found" % (path.name, match[0]))
    if re.search(rgx, content):
        content = add_include(content, meta, origin, keys)
    return content


def add_meta(content, meta, origin, keys):
    rgx = r"\$m:(?P<key>[\w:]+)"
    for match in re.finditer(rgx, content):
        if match["key"] in meta:
            val = meta[match["key"]]
            content = content.replace(match[0], val)
            keys["m"][match["key"]] = val
        else:
            matches = {}
            for key in [k for k in meta.keys() if match["key"] in k]:
                subkey = key.replace(match["key"], "")[1:]
                matches[subkey] = keys["m"][key] = meta[key]
            if matches:
                content = content.replace(match[0], json.dumps(matches))
            else:
                print("[%s]: Key '%s' not found" % (origin, match[0]))
    return content


def make(meta, keys):
    if PUBLIC.exists():
        shutil.rmtree(PUBLIC)
    PUBLIC.mkdir()
    shutil.copytree(ASSETS, PUBLIC / "assets")
    for name, content in PAGES.items():
        page = PUBLIC / "".join(Path(name).suffixes)[1:]
        content = add_include(content, meta, name, keys)
        soup = BeautifulSoup(content, "html.parser")
        for child in soup.find_all(string=lambda e: isinstance(e, Comment)):
            child.extract()
        page.write_text(soup.prettify())
    for key in {k: v for k, v in meta.items() if k not in keys["m"]}:
        print("[%s]: Key '%s' wasn't used" % (META.name, key))


keys = {"m": {}, "i": {}}
meta = get_meta(keys)
make(meta, keys)
