// Adds the map
// First element in markers is the one with the open popup
function init_map () {
  document.querySelectorAll(".territory, .reps div").forEach(el => {
    el.style.fill = ""; // Reset
    el.addEventListener("mouseout", inactive_color);
    el.addEventListener("mouseover", active_color);
    el.addEventListener("click", show);
  });
}

function inactive_color(el) {
  document.querySelectorAll(".territory, .reps div").forEach(territory => {
    territory.classList.remove("active");
  });
}

function active_color(el) {
  el = el.currentTarget ? el.currentTarget : el;
  inactive_color(el);
  el.classList.add("active");
  if (el.nodeName == "DIV") {
    territories = el.dataset.territories.split(", ");
    document.querySelectorAll(".territory").forEach(territory => {
      if (territories.includes(territory.id)) {
        territory.classList.add("active");
      }
    });
  } else {
    document.querySelectorAll(".reps div").forEach(rep => {
      territories = rep.dataset.territories.split(", ");
      if (territories.includes(el.id)) {
        rep.classList.add("active");
      }
    });
  }
}

function show(el) {
  el = el.currentTarget ? el.currentTarget : el;
  active_color(el);
}

init_map();
