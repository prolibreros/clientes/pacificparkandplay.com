// Inits logo animation
function init_logo () {

  // Gets current opacity of DOM element
  function get_initial_opacity (element, reverse) {
    var opacity = parseFloat(element.style.opacity);
    if (isNaN(opacity)) {
      return reverse ? 1.0 : 0.0;
    } else {
      return opacity;
    }
  }

  // Changes opacity of DOM elements
  function change_opacity (element, amount = .1, interval = 50, reverse = false) {
    let opacity = get_initial_opacity(element, reverse),
        condition = reverse ? opacity > 0 : opacity < 1,
        quantity = reverse ? opacity - amount : opacity + amount;
    if (condition) {
      element.classList.add('busy');
      element.style.opacity = quantity;
      setTimeout(() => {
        change_opacity(element, amount, interval, reverse);
      }, interval);
    } else { element.classList.remove('busy') }
  }

  // Inits animation effect on intro
  function init_animation () {
    let intro = document.getElementById('intro'),
        arrow = document.getElementById('arrow');
    // new Parallax(intro);
    document.body.classList.remove('wait');
    change_opacity(intro, .1, 50);
    change_opacity(arrow, .1, 50);
  }

  let url = new URL(document.URL),
    params = new URLSearchParams(url.search),
    svg = document.querySelector('svg'),
    // Last element that animates, in this case is the element 36
    last = document.querySelector('.animate-36');
  svg.classList.add('active');
  last.addEventListener('webkitTransitionEnd', init_animation);
  last.addEventListener('transitionend', init_animation);

  setTimeout(() => {
    document.getElementById('enter-btn').click();
  }, 8000);
}

init_logo();
