// Allows sending emails
function init_form () {
  const form = document.getElementById('form');
  emailjs.init('qkQRvJZQV_c5y-kO1')
  form.addEventListener('submit', function(event) {
    const btn = document.getElementById('submit'),
          div = document.getElementById('sended'),
          serviceID = 'pacificparkandplay',
          templateID = 'website_contact';
    event.preventDefault();
    btn.value = 'Sending...';
    emailjs.sendForm(serviceID, templateID, this).then(() => {
      btn.value = 'Send';
      form.reset();
      form.classList.toggle('hidden');
      div.classList.toggle('hidden');
    }, (err) => {
      btn.value = 'Send';
      alert(JSON.stringify(err));
    });
  });
}

init_form();
