// Starts the testimonials slider
function init_slider () {
  var slider = new Swiper('.mySwiper', {
    slidesPerView: 1,
    spaceBetween: 10,
    loop: true,
    keyboard: { enabled: true },
    pagination: {
      type: 'progressbar',
      el: '.swiper-pagination'
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    //breakpoints: {
    //  640: { slidesPerView: 2 },
    //  1024: { slidesPerView: 3 },
    //},
  });
}

init_slider();
