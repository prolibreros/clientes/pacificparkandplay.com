# Goes to root directory
cd $(dirname $0)/..

while inotifywait -e modify -e delete -e create -qr .; do
  sh scripts/build.sh
done
